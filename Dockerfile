FROM igwn/lalsuite-dev:el7

ARG BUILD_DATE
ARG CI_COMMIT_SHA
ARG CI_JOB_TOKEN

# http://label-schema.org/rc1/
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="ScoX1CrrossCorr"
LABEL org.label-schema.description="Cross correlation analysis for O3 ScoX1"
LABEL org.label-schema.url="https://git.ligo.org/LMXBCrossCorr"
LABEL org.label-schema.vcs-url="https://git.ligo.org/james-clark/scox1-crosscorr"
LABEL org.label-schema.build-date="${BUILD_DATE}"
LABEL org.label-schema.vcs-ref="${CI_COMMIT_SHA}"

# Setup directories for binding
RUN mkdir -p /cvmfs /hdfs /gpfs /ceph /hadoop /etc/condor

RUN yum upgrade -y && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN  git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@git.ligo.org/LMXBCrossCorr/lalsuite.git && \
      cd lalsuite && \
      git checkout -b crosscorr-lattice-dev-clean origin/crosscorr-lattice-dev-clean && \
      ./00boot  && \
      ./configure --prefix /opt/lscsoft/lalsuite --enable-swig-python --disable-lalstochastic --disable-laldetchar && \
      make -j && \
      make install && \
      cd / && \
      rm -rf lalsuite

RUN ["/bin/bash", "-c", "source /opt/lscsoft/lalsuite/etc/lalsuiterc"]

COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]
